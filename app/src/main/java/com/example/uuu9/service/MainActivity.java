package com.example.uuu9.service;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {

    final String LOG_TAG = "myLogs";



    public final static String test_res = "test";
    public final static String PARAM_TASK = "task";
    public final static String PARAM_STATUS = "status";

    public final static String BROADCAST_ACTION = "ru.startandroid.develop.p0961servicebackbroadcast";



    BroadcastReceiver br;
    EditText et;
    ListView lv;
    ArrayList<Map<String, String>> data;
    SimpleAdapter aa;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et=(EditText)findViewById(R.id.edit);
        lv=(ListView)findViewById(R.id.lv);
        data=new ArrayList<>();
        aa = new SimpleAdapter(this, data, R.layout.item,
                new String[]{"me", "bot"}, new int[]{R.id.me, R.id.nameTV});
        lv.setAdapter(aa);

        br = new BroadcastReceiver() {

            public void onReceive(Context context, Intent intent) {
                int task = intent.getIntExtra(PARAM_TASK, 0);
                int status = intent.getIntExtra(PARAM_STATUS, 0);
                Log.d(LOG_TAG, "onReceive: task = " + task + ", status = " + status);
                String t =intent.getStringExtra("request");
                if(t.equals("stop")){unregisterReceiver(br);t="bye";}
                if(t!=null){
                    Map<String, String> map = new HashMap();
                    map.put("me",et.getText().toString());
                    map.put("bot",t);
                    data.add(map);
                    aa.notifyDataSetChanged();}

            }
        };

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(br, intFilt);
    }



    public void onClickStart(View v) {
        if(et.getText().toString().equals("hi")){
            IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
            registerReceiver(br, intFilt);
        }
        else if(et.getText().equals("bye"))unregisterReceiver(br);
        Intent intent;
        intent = new Intent(this, MyService1.class).putExtra("text", et.getText().toString());
        startService(intent);

    }

}
//    final String LOG_TAG = "myLogs";
//
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//    }
//
//    public void onClickStart(View v) {
//        startService(new Intent(this, MyService1.class));
//    }
//
//    public void onClickStop(View v) {
//        stopService(new Intent(this, MyService1.class));
//    }
//public void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_main);
//}
//
//    public void onClickStart(View v) {
//        startService(new Intent(this, MyService1.class).putExtra("time", 7));
//        startService(new Intent(this, MyService1.class).putExtra("time", 2));
//        startService(new Intent(this, MyService1.class).putExtra("time", 4));
//    }
//}
