package com.example.uuu9.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MyService1 extends Service {

    final String LOG_TAG = "myLogs";
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "MyService onCreate");
        es = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "MyService onStartCommand");
        String text = intent.getStringExtra("text");
        MyRun mr = new MyRun(startId, text);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {

        int time;
        int startId;
        int task;
        String text;

        public MyRun(int startId, String text) {
            this.text=text;
            this.startId = startId;
            Log.d(LOG_TAG, "MyRun#" + startId + " create");
        }

        public void run() {
            Intent intent = new Intent(MainActivity.BROADCAST_ACTION);
            Log.d(LOG_TAG, "MyRun#" + startId + " start, time = " + time);
            if(text.equals("hi"))
                intent.putExtra("request", "hi");
            else if(text.equals("bye"))intent.putExtra("request","stop");
            else intent.putExtra("request", "I didn't understand");
            int time = (int) (1 + (Math.random() * 4));
            try {
                Thread.sleep(time*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sendBroadcast(intent);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(MyService1.this)
                            .setSmallIcon(android.R.drawable.presence_audio_away)
                            .setContentTitle("My notification")
                            .setContentText("Hello World!");
            NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
               manager.notify(0,mBuilder.build());

            stop();
        }

        void stop() {
            Log.d(LOG_TAG, "MyRun#" + startId + " end, stopSelfResult("
                    + startId + ") = " + stopSelfResult(startId));
        }
    }
}

//    void someTask() {
//        new Thread(new Runnable() {
//            public void run() {
//                for (int i = 1; i<=5; i++) {
//                    Log.d(LOG_TAG, "i = " + i);
//                    try {
//                        TimeUnit.SECONDS.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                stopSelf();
//            }
//        }).start();
//    }
